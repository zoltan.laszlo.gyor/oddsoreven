package main;

import java.util.Random;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		System.out.println("Let�s play a game called �Odds and Evens�");
		Scanner sc = new Scanner(System.in);
		System.out.println("What is your name?");
		String name = sc.nextLine();
		System.out.println("Hi " + name + ", which one do you choose? Odds or Evens?");
		String decision = sc.next();
		if(decision.equals("Odds")  || decision.equals("odds")) {
			System.out.println("Alright! " + name + " has picked odds and the computer will be evens");
		}else{
			System.out.println("Alright! " + name + " has picked evens and the computer will be odds");
		}
		System.out.println("-------------");
		
		System.out.println("How many �fingers� do you put out?");
		int fingers = sc.nextInt();
		System.out.println("You play " + fingers + " \"fingers\".");
		Random random = new Random();
		int computerChoice = random.nextInt(6);
		System.out.println("The computer plays " + computerChoice + " \"fingers\".");
		System.out.println("-------------");
		
		int sum = fingers + computerChoice;
		System.out.println("The summary: " +sum);
		
		boolean odds = true;
		if(sum % 2 == 0) {
			odds = false;
			System.out.println(sum + " is even!");
		}else {
			odds = true;
			System.out.println(sum + " is odd!");
		}
		System.out.println("-------------");
		
		if(odds) {
			if(decision.equals("Odds") || decision.equals("odds")) {
				System.out.println(name +" wins");
			}else {
				System.out.println("The computer wins");
			}
		}else {
			if(decision.equals("evens") || decision.equals("Evens")) {
				System.out.println(name +" wins");
			}else {
				System.out.println("The computer wins");
			}
		}
	}
}
